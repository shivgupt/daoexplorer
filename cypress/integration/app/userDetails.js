  describe('User Details page', () => {
  let user = '0xada083a3c06ee526F827b43695F2DcFf5C8C892B'
    beforeEach(() => {
      cy.visit(`/user/${user}`)
    })

    it('Should navigate to proposed proposal details on click', () => {
      cy.get('div#ProposalTable').within(($div) => {
        cy.get('a[role="button"]').then(($list) => {
          cy.wrap($list[0]).click()
          cy.url().should('include', '/proposal')
        })
      })
    })

    it('Should navigate to voted proposal details', () => {
      cy.get('div#VoteTable').within(($div) => {
        cy.get('a[role="button"]').then(($list) => {
          cy.wrap($list[0]).click()
          cy.url().should('include', '/proposal')
        })
      })
    })

    it('Should navigate to staked proposal details on click', () => {
      cy.get('div#StakeTable').within(($div) => {
        cy.get('a[role="button"]').then(($list) => {
          cy.wrap($list[0]).click()
        })
      })
      cy.url().should('include', '/proposal')
      cy.get('p').contains('Proposal:')
    })
  })
