  describe('Proposal Details page', () => {
  let proposal = '0xfa923bfdc5311d43500d591e7a2bafbc4c8c12e44dea9db019b71f699e16e6af'
    beforeEach(() => {
      cy.visit(`/proposal/${proposal}`)
    })

    it('Should navigate to proposer details on click', () => {
      cy.get('p:contains(Created By)').within(($p) => {
        cy.get('a').then(($ref) => {
          cy.wrap($ref[1]).click()
        })
      })
      cy.url().should('include', '/user')
    })

    it('Should navigate to voter details', () => {
      cy.get('div#VoterTable').within(($div) => {
        cy.get('a[role="button"]').then(($list) => {
          cy.wrap($list[0]).click()
          cy.url().should('include', '/user')
        })
      })
    })

    it('Should navigate to staker details on click', () => {
      cy.get('div#StakerTable').within(($div) => {
        cy.get('a[role="button"]').then(($list) => {
          cy.wrap($list[0]).click()
          cy.url().should('include', '/user')
        })
      })
    })
  })
