describe('DAOexplorer index', () => {
  let user = '0xada083a3c06ee526F827b43695F2DcFf5C8C892B'
  let proposal = '0xfa923bfdc5311d43500d591e7a2bafbc4c8c12e44dea9db019b71f699e16e6af'

  describe('Route to given url', () => {
    it ('Visit /home', () => {
      cy.visit('/home')
      cy.get('p').should('contain', 'Genesis Alpha')
    })

    it('Visit /proposals', () => {
      cy.visit('/proposals')
      cy.get("strong:contains(Status)").its('length').should('be.gte', 1)
    })

    it('Visit /users', () => {
      cy.visit('/users')
      cy.get("strong:contains(Proposed)").its('length').should('be.gte', 1)
    })

    it('Visit /user/:id', () => {
      cy.visit(`/user/${user}`)
      cy.get('p').contains(/0xada083a3c06ee526F827b43695F2DcFf5C8C892B/i)
    })

    it('Visit /proposal/:id', () => {
      cy.visit(`/proposal/${proposal}`)
    })
  })

  describe('Update url and open page on button click', () => {
    it('Should change display->Home and url->/home on Click Home', () => {
      cy.visit('/')
      cy.get("a#home_button").click()
      cy.url().should('include', '/home')
      cy.get('p').should('contain', 'Genesis Alpha')
    })

    it('Should change display->Proposals and url->/proposals on Click Proposals', () => {
      cy.visit('/')
      cy.get("a#viewProposals_button").click()
      cy.url().should('include', '/proposals')
      cy.get("strong:contains(Status)").its('length').should('be.gte', 1)
    })

    it('Should change display->Users and url->/users on Click Users', () => {
      cy.visit('/')
      cy.get("a#viewUsers_button").click()
      cy.url().should('include', '/users')
      cy.get("strong:contains(Proposed)").its('length').should('be.gte', 1)
    })

    it.only('Should change display->User:id Details and url->/user/:id on Click user info', () => {
      cy.visit('/users')
      cy.get('main > div > div > div > div > a[role="button"]').then(($list) => {
        cy.wrap($list[0]).click()
      })
    })

    it('Should change display->Proposal:id Details and url->/proposal/:id on Click proposal info', () => {
      cy.visit('/proposals')
      cy.get('main > div > div > div > a[role="button"]').then(($list) => {
        cy.wrap($list[0]).click()
      })
    })
  })
})
