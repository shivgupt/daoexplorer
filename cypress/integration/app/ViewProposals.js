import _ from 'lodash';

const testSortByRep = () => {
  let values = []
  cy.get("li#sort_rep").click()
  cy.get('p:contains(Rep)')
    .each(($el, i, $list) => {
      if (i != 0) {
        var result = $el.text().match(/, [0-9]+.*[0-9]* Rep/g)
        if (result) {
          var temp = result[0].split(" ")
          values.push(Number(temp[1]))
        }
      }
    })
    .then(() => {
      var sorted = _.orderBy(values, 'desc')
      expect(values).deep.equal(sorted)
    })
}

const testSortByEth = () => {
  let values = []
  cy.get("li#sort_eth").click()
  cy.get('p:contains(ETH)')
    .each(($el, i, $list) => {
      if (i != 0) {
        var result = $el.text().match(/: [0-9]+.*[0-9]* ETH,/)
        if (result) {
          var temp = result[0].split(" ")
          values.push(Number(temp[1]))
        }
      }
    })
    .then(() => {
      var sorted = _.orderBy(values, 'desc')
      expect(values).deep.equal(sorted)
    })
}

const testSortByStake = () => {
  let values = []
  cy.get("li#sort_stake").click()
  cy.get('p:contains(Stake)')
    .each(($el, i, $list) => {
      if (i != 0) {
        var result = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
        if (result) {
          var temp = result[0].split(" ")
          values.push(Number(temp[1]) + Number(temp[2]))
        }
      }
    })
    .then(() => {
      var sorted = values.sort((a, b) => {return b-a})
      expect(values).deep.equal(sorted)
    })
}

const testSortByDate = () => {
  let values = []
  cy.get("li#sort_date").click()
  cy.get('p:contains(Submitted On:)')
    .each(($el, i, $list) => {
      if (i != 0) {
        var result = $el.text().match(/[a-z]+ [a-z]+ [0-9]+ [0-9]+/gi)
        if (result) {
          values.push((new Date(result)).getTime()/1000)
        }
      }
    })
    .then(() => {
      var sorted = _.orderBy(values, 'desc')
      expect(values).deep.equal(sorted)
    })
}

describe('Proposal page', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.get("button#viewProposals_button").click()
    //cy.contains('h2', /loading/i).should('exist', { timeout: 4000 })
    cy.get("li#sort_rep").should('exist')
  })

  describe('Filter passed proposals', () => {
    describe('only boosted proposals that are passed', () => {
      beforeEach(() => {
        // Turn On
        cy.get("li#filter_passed").click()
        // Turn Off
        cy.get("li#filter_awaiting").click()
      })

      it('should filter proposals', () => {
        var result = null
        var stake = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Failed/g) || $el.text().match(/Status: Awaiting/g)
              if (result) {
                return
              }
              var stakes = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
              if (stakes) {
                var temp = stakes[0].split(" ")
                if (Number(temp[1]) <= 0) {
                  stake = temp[1]
                }
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
            expect(stake).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
    describe('only non boosted proposals that are passed', () => {
      beforeEach(() => {
        // Turn On
        cy.get("li#filter_passed").click()
        cy.get("li#filter_nonboosted").click()

        // Turn Off
        cy.get("li#filter_boosted").click()
        cy.get("li#filter_awaiting").click()
      })

      it('should filter proposals', () => {
        var result = null
        var stake = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Failed/g) || $el.text().match(/Status: Awaiting/g)
              if (result) {
                return
              }
              var stakes = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
              if (stakes) {
                var temp = stakes[0].split(" ")
                if (Number(temp[1]) > 0) {
                  stake = temp[1]
                }
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
            expect(stake).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
    describe('both boosted and non boosted proposals that are passed', () => {
      beforeEach(() => {
        // Turn off
        cy.get("li#filter_awaiting").click()

        // Turn on
        cy.get("li#filter_passed").click()
        cy.get("li#filter_nonboosted").click()
      })

      it('should filter proposals', () => {
        var result = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Failed/g) || $el.text().match(/Status: Awaiting/g)
              if (result) {
                return
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
  })

  describe('Filter failed proposals', () => {
    describe('only boosted proposals that are failed', () => {
      beforeEach(() => {
        // Turn On
        cy.get("li#filter_failed").click()
        // Turn Off
        cy.get("li#filter_awaiting").click()
      })

      it('should filter proposals', () => {
        var result = null
        var stake = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Passed/g) || $el.text().match(/Status: Awaiting/g)
              if (result) {
                return
              }
              var stakes = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
              if (stakes) {
                var temp = stakes[0].split(" ")
                if (Number(temp[1]) <= 0) {
                  stake = temp[1]
                }
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
            expect(stake).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
    describe('only non boosted proposals that are failed', () => {
      beforeEach(() => {
        // Turn On
        cy.get("li#filter_failed").click()
        cy.get("li#filter_nonboosted").click()

        // Turn Off
        cy.get("li#filter_boosted").click()
        cy.get("li#filter_awaiting").click()
      })

      it('should filter proposals', () => {
        var result = null
        var stake = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Passed/g) || $el.text().match(/Status: Awaiting/g)
              if (result) {
                return
              }
              var stakes = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
              if (stakes) {
                var temp = stakes[0].split(" ")
                if (Number(temp[1]) > 0) {
                  stake = temp[1]
                }
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
            expect(stake).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
    describe('both boosted and non boosted proposals that are failed', () => {
      beforeEach(() => {
        // Turn off
        cy.get("li#filter_awaiting").click()

        // Turn on
        cy.get("li#filter_failed").click()
        cy.get("li#filter_nonboosted").click()
      })

      it('should filter proposals', () => {
        var result = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Passed/g) || $el.text().match(/Status: Awaiting/g)
              if (result) {
                return
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
  })

  describe('Filter awaiting proposals', () => {
    describe('only boosted proposals that are awaiting', () => {

      it.only('should filter proposals', () => {
        var result = null
        var stake = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Failed/g) || $el.text().match(/Status: Passed/g)
              if (result) {
                return
              }
              var stakes = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
              if (stakes) {
                var temp = stakes[0].split(" ")
                if (Number(temp[1]) <= 0) {
                  stake = temp[1]
                }
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
            expect(stake).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
    describe('only non boosted proposals that are awaiting', () => {
      beforeEach(() => {
        // Turn On
        cy.get("li#filter_nonboosted").click()

        // Turn Off
        cy.get("li#filter_boosted").click()
      })

      it('should filter proposals', () => {
        var result = null
        var stake = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Failed/g) || $el.text().match(/Status: Passed/g)
              if (result) {
                return
              }
              var stakes = $el.text().match(/Stake\s+[0-9]+.*[0-9]*\s+[0-9]+.*[0-9]*/g)
              if (stakes) {
                var temp = stakes[0].split(" ")
                if (Number(temp[1]) > 0) {
                  stake = temp[1]
                }
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
            expect(stake).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
    describe('both boosted and non boosted proposals that are awaiting', () => {
      beforeEach(() => {
        // Turn on
        cy.get("li#filter_nonboosted").click()
      })

      it('should filter proposals', () => {
        var result = null
        cy.get('p:contains(Status: )')
          .each(($el, i, $list) => {
            if (i != 0) {
              result = $el.text().match(/Status: Failed/g) || $el.text().match(/Status: Passed/g)
              if (result) {
                return
              }
            }
          })
          .then(() => {
            expect(result).to.be.null
          })
      })

      it('should sort filtered proposals by rep payout' , () => {
        testSortByRep()
      })

      it('should sort filtered proposals by stake' , () => {
        testSortByStake()
      })

      it('should sort filtered proposals by eth payout' , () => {
        testSortByEth()
      })

      it('should sort filtered proposals by date' , () => {
        testSortByDate()
      })
    })
  })
})
