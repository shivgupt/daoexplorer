# get all proposals

```
{
  proposals ( where:{ proposalId_not: "" }) {
    proposalId
  }
}
```

# Get proposals for some Avatar

```
{
  dao (id: "0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"){
    proposals {
      proposalId
      decision
    }
  }
}
```

## Get all proposals proposed by some account

```
{
  accounts ( where: { address: "0x0017cd246be69d243657400685b3c17c545bfd0f" }) {
    proposals {
      proposalId
    }
  }
}
```

## Get all proposals voted on by some account

```
{
  accounts ( where: { address: "0x0017cd246be69d243657400685b3c17c545bfd0f" }) {
    votes {
      proposal {
        proposalId
      }
    }
  }
}
```

## Get all proposals staked on by some account

```
{
  accounts ( where: { address: "0x0017cd246be69d243657400685b3c17c545bfd0f" }) {
    stakes {
      proposal {
        proposalId
      }
    }
  }
}
```

## Get all members of some DAO

```
{
  dao (id: "0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"){
    members {
      address
    }
  }
}
```

## Get all redemptions for some account

```
{
  accounts ( where: { address: "0x0017cd246be69d243657400685b3c17c545bfd0f" }) {
    redemptions {
      redemptionId
      type
      amount
    }
  }
}
```

## Get all redemptions on some proposal

```
{
  proposals (id: "0xa8c1561325ddb817cfbf5adbe3dfd043d2e54158a218dddf0b55b040f6b21cf3") {
    redemptions{
      redemptionId
      type
      amount
    }
  }
}
```

## Get all details for some proposalId

```
{
  proposals (id: "0xa8c1561325ddb817cfbf5adbe3dfd043d2e54158a218dddf0b55b040f6b21cf3") {
    proposalId
    proposer {
      address
    }
    dao {
      avatarAddress
    }
    decision
    stakes {
      staker {
        address
      }
      stakeAmount
      prediction
    }
    votes {
      voter {
        address
      }
      voteOption
    }
    redemptions {
      proposal {
        proposalId
      }
      account {
        address
      }
      type
      amount
    }
  }
}
```

## Get all details for some account

```
{
  accounts ( where: { address: "0x0017cd246be69d243657400685b3c17c545bfd0f" }) {
    accountId
    dao{
      avatarAddress
    }
    address
    hasReputation
    stakes{
      proposal{
          proposalId
      }
      prediction
    }
    votes{
      proposal{
          proposalId
      }
      voteOption
    }
    proposals{
      proposalId
    }
    redemptions {
      redemptionId
      type
      amount
    }
  }
}

```
