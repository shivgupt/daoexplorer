FROM daoexplorer_builder:dev as base
FROM graphprotocol/graph-node:latest
WORKDIR /root
ENV HOME /root
RUN apt-get update && apt-get install -y bash curl jq && apt-get upgrade -y
COPY --from=base /ops /ops
COPY ops ops
COPY build build
ENTRYPOINT ["bash", "ops/entry.sh"]
