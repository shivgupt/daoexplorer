#!/bin/bash
set -e

addresses="ops/addresses.json"
if [[ ! -f $addresses ]]
then echo "Fatal: Couldn't find addresses file ($addresses)" && exit 1
fi

targets="`cat $addresses | jq keys | tr -d '",[] ' | tr '\n' ' '`"
for target in $targets
do
  artifacts=build/contracts/$target.json
  backup=build/contracts/$target.backup.json
  mv $artifacts $backup
  jq -s ".[0].$target * .[1]" $addresses $backup > $artifacts
  rm $backup
done

