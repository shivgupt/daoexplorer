// this migration file is used only for testing purpose
var AbsoluteVote = artifacts.require('./AbsoluteVote.sol');
var ContributionReward = artifacts.require('./ContributionReward.sol');
var ControllerCreator = artifacts.require('./ControllerCreator.sol');
var DaoCreator = artifacts.require('./DaoCreator.sol');
var DaoToken = artifacts.require('./DaoToken.sol');
var GenesisProtocol = artifacts.require('./GenesisProtocol.sol');
var GlobalConstraintRegistrar = artifacts.require('./GlobalConstraintRegistrar.sol');
var QuorumVote = artifacts.require('./QuorumVote.sol');
var SchemeRegistrar = artifacts.require('./SchemeRegistrar.sol');
var SimpleICO = artifacts.require('./SimpleICO.sol');
var TokenCapGC = artifacts.require('./TokenCapGC.sol');
var UpgradeScheme = artifacts.require('./UpgradeScheme.sol');
var VestingScheme = artifacts.require('./VestingScheme.sol');
var VoteInOrganizationScheme = artifacts.require('./VoteInOrganizationScheme.sol');
var OrganizationRegister = artifacts.require('./OrganizationRegister.sol');
var Redeemer = artifacts.require('./Redeemer.sol');
var UController = artifacts.require('./UController.sol');

const options = { gas: 6100000 };

// Stupid truffle using stupid old version of web3
const toWei = web3.utils ? web3.utils.toWei : web3.toWei;

const cap = toWei("100000000","ether");

module.exports = function (deployer, network, accounts) {
  return (deployer.then(async function() {

    // Deploy GEN token
    await deployer.deploy(DaoToken, "DAOstack", "GEN", cap, options);
    var genToken = await DaoToken.deployed();

    // Deploy Creators
    await deployer.deploy(ControllerCreator, options)
    var controllerCreator = await ControllerCreator.deployed();
    await deployer.deploy(DaoCreator, controllerCreator.address, options);

    // Deploy Universal Controller
    await deployer.deploy(UController, options);

    // Deploy Voting Machines
    await deployer.deploy(GenesisProtocol, genToken.address, options);
    await deployer.deploy(AbsoluteVote, options);
    await deployer.deploy(QuorumVote, options);

    // Deploy Universal Schemes
    await deployer.deploy(SchemeRegistrar, options);
    await deployer.deploy(UpgradeScheme, options);
    await deployer.deploy(GlobalConstraintRegistrar, options);
    await deployer.deploy(ContributionReward, options);
    await deployer.deploy(SimpleICO, options);
    await deployer.deploy(VestingScheme, options);
    await deployer.deploy(VoteInOrganizationScheme, options);
    await deployer.deploy(OrganizationRegister, options);

    // Deploy Universal Global Contraints
    await deployer.deploy(TokenCapGC)

    // Deploy Redeemer
    const genesisProtocolInstance = await GenesisProtocol.deployed();
    const contributionRewardInstance = await ContributionReward.deployed();
    await deployer.deploy(Redeemer,
      contributionRewardInstance.address,
      genesisProtocolInstance.address,
      options
    );

  }));
}
