FROM alpine:3.8
WORKDIR /root
ENV HOME /root

RUN apk add --update --no-cache bash certbot nginx openssl && \
    openssl dhparam -out /etc/ssl/dhparam.pem 2048 && \
    ln -fs /dev/stdout /var/log/nginx/access.log && \
    ln -fs /dev/stdout /var/log/nginx/error.log

COPY ops/wait-for.sh /root/wait-for.sh
COPY modules/proxy/entry.sh /root/entry.sh
COPY modules/proxy/dev.conf /etc/nginx/nginx.conf

COPY modules/proxy/graphiql /var/www/html/graphiql

ENTRYPOINT ["bash", "/root/entry.sh"]
