import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// UI library
import AppBar from '@material-ui/core/AppBar';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { fade } from '@material-ui/core/styles/colorManipulator';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

// Icons
import Docs from '@material-ui/icons/Assignment';
import Home from '@material-ui/icons/HomeRounded';
import Menu from '@material-ui/icons/Menu';
import People from '@material-ui/icons/People';
import SearchIcon from '@material-ui/icons/Search';

const drawerWidth = 100;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
});

class NavBar extends Component {
  render() {

    const {viewProposals, viewHome, viewUsers, classes, theme} = this.props

    return (
      <AppBar
        color="inherit"
        position="fixed"
        className={classes.appBar}
      >
        <Toolbar>
          <IconButton component={Link} to={"/home"} id="home_button" color="secondary" onClick={() => viewHome()}>
            <Home/>
          </IconButton>
          <IconButton component={Link} to={"/proposals"} id="viewProposals_button" color="secondary" onClick={() => viewProposals()}>
            <Docs/>
          </IconButton>
          <IconButton component={Link} to={"/users"} id="viewUsers_button" color="secondary" onClick={() => viewUsers()}>
            <People/>
          </IconButton>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Coming Soon..."
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
            />
          </div>
        </Toolbar>
      </AppBar>
    )
  }
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(NavBar);
