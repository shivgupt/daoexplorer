import React from 'react';
import { User } from './User';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    textAlign: 'center',
  },
  body: {
    fontSize: 14,
    textAlign: 'center',
  },
}))(TableCell);

class UserTable extends React.Component {
  render() { 
    const { alchemyData, viewUser } = this.props
    if (!alchemyData) return null
    console.log(`alchemyData = ${JSON.stringify(this.props.alchemyData, null, 2)}`)
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <CustomTableCell> Users </CustomTableCell>
              <CustomTableCell> More </CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.rowdata.map( user => (
              <User user={user} viewUser={this.props.viewUser}/>
            ))}
          </TableBody>
        </Table>
      </Paper>
    )
  }
}

export { UserTable }
