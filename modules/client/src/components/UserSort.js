import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// Material UI icons
import Boosted from '@material-ui/icons/OfflineBoltOutlined';
import Docs from '@material-ui/icons/Assignment';
import Payout from '@material-ui/icons/AttachMoneyRounded';
import Star from '@material-ui/icons/Star';
import Time from '@material-ui/icons/AccessTimeRounded';
import Vote from '@material-ui/icons/ThumbsUpDownRounded';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  listIcon: {
    marginRight: 8,
    marginLeft: 1,
  },
  sidebarText: {
    fontSize: 11,
  },
  padding: {
    padding: 0,
  },
});


class UserSort extends React.Component {
  render() {
  const { handleSortChange, classes, sortOption } = this.props;
    return (
      <List dense={true}>
        <ListItem key={"Last Seen"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Time/>} icon={<Time/>} value="time" onChange={handleSortChange} checked={sortOption === 'time'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Last Seen</Typography>} />
        </ListItem>
        <ListItem key={"# Proposals"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Docs/>} icon={<Docs/>} value="numProp" onChange={handleSortChange} checked={sortOption === 'numProp'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}># Proposals</Typography>} />
        </ListItem>
        <ListItem key={"# Staked"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Boosted/>} icon={<Boosted/>} value="numStake" onChange={handleSortChange} checked={sortOption === 'numStake'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}># Staked</Typography>} />
        </ListItem>
        <ListItem key={"# Voted"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Vote/>} icon={<Vote/>} value="numVote" onChange={handleSortChange} checked={sortOption === 'numVote'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}># Voted</Typography>} />
        </ListItem>
        <ListItem key={"Reputation"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Star/>} icon={<Star/>} value="rep" onChange={handleSortChange} checked={sortOption === 'rep'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Reputation</Typography>} />
        </ListItem>
      </List>
    )
  }
}

UserSort.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserSort);
