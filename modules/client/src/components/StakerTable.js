import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import BN from 'bignumber.js';

const normalize = new BN('1000000000000000000')

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tableHeader: {
    backgroundColor: theme.palette.common.black,
    fontSize: 10,
    color: 'white',
  },
  tableContent: {
    padding: theme.spacing.unit * 2,
    fontSize: 8,
  },
});

class StakerTable extends React.Component {
  state = {
    spacing: '16',
  };

  render() {
    const { spacing } = this.state;
    const { classes } = this.props;
    const { stakes, viewUser, alchemyData } = this.props
    if (!stakes) return null
    return (
      <Grid id='StakerTable' container className={classes.root} spacing={16}>
        <Grid item xs={6} className={classes.tableHeader}>
          <Typography gutterBottom noWrap className={classes.tableHeader}>
            Staker
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Amount
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Prediction
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            More
          </Typography>
        </Grid>
        {
          stakes.map( stake => (
          <Grid key={stake.staker.address} container className={classes.root} spacing={16}>
            <Grid item xs={6}>
              <Typography gutterBottom className={classes.tableContent}>
                {stake.staker.address}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography gutterBottom className={classes.tableContent}>
                {new BN(stake.stakeAmount).dividedBy(normalize).toString()}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography gutterBottom className={classes.tableContent}>
                {stake.prediction === "1" ? 'For' : 'Against'}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <IconButton onClick={() => viewUser(stake.staker.address)}>
                <InfoIcon />
              </IconButton>
            </Grid>
          </Grid>
          ))
        }
      </Grid>
    )
  }
}

StakerTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StakerTable);
