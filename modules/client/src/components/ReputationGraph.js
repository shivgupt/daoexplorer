import React from 'react';
import LineChart, { parseFlatArray } from 'react-linechart';
import _ from 'lodash';
import BN from 'bignumber.js';

class ReputationGraph extends React.Component {
  aggregateRepByTimestamps(daoRepChanges) {
    let result = [{
      time: daoRepChanges[0].time,
      amount: new BN(daoRepChanges[0].amount),
      totalSupplyAfter: new BN(daoRepChanges[0].amount)
    }]
    for (let i = 1, j = 0; i < daoRepChanges.length; i++) {
      if (daoRepChanges[i].time == result[j].time) {
        result[j].amount = result[j].amount.plus(new BN(daoRepChanges[i].amount))
        result[j].totalSupplyAfter = result[j].totalSupplyAfter.plus(new BN(daoRepChanges[i].amount))
      } else {
        result.push({
          time: daoRepChanges[i].time,
          amount: new BN(daoRepChanges[i].amount),
          totalSupplyAfter: result[j].totalSupplyAfter.plus(new BN(daoRepChanges[i].amount))
        })
        j++
      }
    }
    return result
  }
  
  onGroupedHover(point) {
		const formatted = new Date(point.x *1000);
		return `<b>Date: </b>${formatted}<br /><b>Reputation: </b>${point.y}`;
  }

  getGraphData(daoRepChanges, accountRepChanges) {
    let updatedDaoRepChanges = _.takeRightWhile(daoRepChanges, function(o) { return o.time >= accountRepChanges[0].time} )
    let graph = []
    let A = accountRepChanges[0].amount
    let B = updatedDaoRepChanges[0].totalSupplyAfter

    graph.push( { x: A.dividedBy(B).multipliedBy(new BN(100)).decimalPlaces(6), y: accountRepChanges[0].time })
    let i=1, j=1

    for (i,j; i < accountRepChanges.length; j++)
    {
      if (updatedDaoRepChanges[j].time == accountRepChanges[i].time) {
        A = A.plus(accountRepChanges[i].amount)
        i++
      }
      graph.push({x: A.dividedBy(updatedDaoRepChanges[j].totalSupplyAfter).multipliedBy(new BN(100)).decimalPlaces(6), y: updatedDaoRepChanges[j].time})
    }

    for ( i,j; j < updatedDaoRepChanges.length; j++)
    {
      graph.push({x: A.dividedBy(updatedDaoRepChanges[j].totalSupplyAfter).multipliedBy(new BN(100)).decimalPlaces(6), y: updatedDaoRepChanges[j].time})
    }

    //console.log(`graph = ${JSON.stringify(graph, null, 2)}`)
    //console.log(`take while = ${JSON.stringify(updatedDaoRepChanges, null, 2)}`)
    return parseFlatArray(graph, "y", "x")
  }

  render() {
    const { daoRepChanges, accountRepChanges } = this.props
    let data = []
    if (accountRepChanges.length > 0) {
      let daoRepChanges1 = this.aggregateRepByTimestamps(daoRepChanges)
      let accountRepChanges1 = this.aggregateRepByTimestamps(accountRepChanges)
      data = this.getGraphData(daoRepChanges1, accountRepChanges1);
    }

    return (
        <LineChart
          width={300}
          height={300}
          yMin={"0"}
          xLabel={"Time"}
          yLabel={"Reputation %"}
          data={data}
          isDate={true}
          onPointHover={this.onGroupedHover}
          xParser={(d)=> d*1000}
          pointRadius={1}
          hidePoints={false}
          ticks={4}
          interpolate={null}
          showLegends={true}
          interpolate={null}
        />
    )
  }
}

export default ReputationGraph
