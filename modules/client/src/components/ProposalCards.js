import _ from 'lodash';
import BN from 'bignumber.js';
import React from 'react';

import ProposalFilters from './ProposalFilters';
import ProposalSort from './ProposalSort';
import { Link } from "react-router-dom";

// Material UI Core
import AppBar from '@material-ui/core/AppBar';
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import green from '@material-ui/core/colors/green';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import red from '@material-ui/core/colors/red';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

// Material UI icons
import InfoIcon from '@material-ui/icons/Info';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TrendingUp from '@material-ui/icons/TrendingUp';
import TrendingDown from '@material-ui/icons/TrendingDown';
import Star from '@material-ui/icons/Star';
import ThumbDown from '@material-ui/icons/ThumbDownAlt';
import ThumbUp from '@material-ui/icons/ThumbUpAlt';

const drawerWidth = 150;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  colorFor: {
    color: green[400]
  },
  colorAgainst: {
    color: red[700]
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  paper: {
    width: 250,
  },
  toolbar: {
    marginTop: 56,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
});

class ProposalCards extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleSortChange = this.handleSortChange.bind(this)
  }
  state = {
    open: false,
    sortOption: 'time',
    spacing: '8',
    nonboosted: false,
    boosted: true,
    passed: false,
    failed: false,
    awaiting: true,
  };

  handleDrawerOpen = () => {
    this.setState({open: true});
  }

  handleDrawer = () => {
    const { open } = this.state
    console.log(`prev = ${open}`)
    this.setState({open: !open});
  }

  handleSortChange = (event, value) => {
    console.log(`event value ${event.target.value}`)
    console.log(`value = ${value}`)
    this.setState({sortOption: event.target.value})
  }

  handleChange = key => (event, value) => {
    const { nonboosted, boosted, passed, failed, awaiting } = this.state;
    this.setState({
      [key]: value,
    });

  };

  sort(proposals) {
    const { sortOption } = this.state;
    if (sortOption === 'time')
      return _.orderBy(
        proposals,
        [function(proposal) { return proposal.submittedTime }],
        ['desc']
      )
    if (sortOption === 'stake')
      return _.orderBy(
        proposals,
        [function(proposal) {
          // fix the hack to determine boosted threshold
          return ((new BN(proposal.totalStakeFor)).plus(new BN(proposal.totalStakeAgainst))).div(new BN('1000000000000000000')).toNumber()
        }],
        ['desc']
      )
    if (sortOption === 'eth')
      return _.orderBy(
        proposals,
        [function(proposal) { return proposal.ethReward }],
        ['desc']
      )
    if (sortOption === 'rep')
      return _.orderBy(
        proposals,
        [function(proposal) { return proposal.reputationChange }],
        ['desc']
      )
  }

  filterProposals(proposals) {
    const { nonboosted, boosted, passed, failed, awaiting } = this.state;
    const alchemyData = this.props.alchemyData
    return _.filter(proposals, function(proposal) {
      //if (!alchemyData[proposal.proposalId]) return false
      if ((!passed && proposal.decision === "1") ||
          (!failed && proposal.decision === "2") ||
          (!awaiting && proposal.decision === "0") ||
          (!boosted && proposal.totalStakeFor > 0) ||
          (!nonboosted && proposal.totalStakeFor <= 1000000000000000000)
      ) return false
      return true
    })
  }

  getFormatedDate(ts) {
    let date = new Date(ts)
    return date.toString().substring(0,15)
  }

  render() {
    const { spacing } = this.state;
    let {
      alchemyData,
      classes,
      proposals,
      theme,
      viewProposal
    } = this.props
    let filtered = this.filterProposals(proposals)
    let sorted = this.sort(filtered)

    //console.log(`Alchemy Data = ${JSON.stringify(alchemyData)}`)

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          color="default"
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: this.state.open,
          })}
        >
        </AppBar>

        <Drawer
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open,
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            }),
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawer}>
              {this.state.open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          <Divider />
          Sort By
          <Divider />
          <ProposalSort
            handleSortChange={this.handleSortChange}
            sortOption={this.state.sortOption}
          />
          <Divider />
          Filters
          <Divider />
          <ProposalFilters
            handleChange={this.handleChange}
            passed={this.state.passed}
            failed={this.state.failed}
            awaiting={this.state.awaiting}
            boosted={this.state.boosted}
            nonboosted={this.state.nonboosted}
          />
        </Drawer>

        <main className={classes.content}>
          <div />
            <Grid container spacing={Number(spacing)}>
              {sorted.map( proposal => (
                  <Grid key={proposal.proposalId} item>
                    <Paper className={classes.paper}>
                      {alchemyData[proposal.proposalId] ? 
                        <a href={alchemyData[proposal.proposalId].description}>{alchemyData[proposal.proposalId].title}</a>
                        : <a href="#"> No Title </a>
                      }
                      <Typography gutterBottom>
                        <br/>
                        <strong>Status:</strong> {proposal.decision === "1" ? 'Passed' : (proposal.decision === "2" ? 'Failed' : 'Awaiting' )}
                        <br/>
                        <strong>Submitted On:</strong> {this.getFormatedDate(proposal.submittedTime * 1000)}
                        <br/>
                        <strong>Payout:</strong> {proposal.ethReward} ETH, {proposal.reputationChange} Rep
                        <br/>
                        <br/>
                        <strong>Vote</strong>&nbsp;
                        <ThumbUp className={classes.colorFor}/> {(new BN(proposal.totalRepFor)).div(new BN('1000000000000000000')).toFixed(2)}&nbsp;
                        <ThumbDown className={classes.colorAgainst}/> {(new BN(proposal.totalRepAgainst)).div(new BN('1000000000000000000')).toFixed(2)}&nbsp;
                        <br/>
                        <strong>Stake</strong>&nbsp;
                        <TrendingUp className={classes.colorFor}/> {(new BN(proposal.totalStakeFor)).div(new BN('1000000000000000000')).toFixed(2)}&nbsp;
                        <TrendingDown className={classes.colorAgainst}/> {(new BN(proposal.totalStakeAgainst)).div(new BN('1000000000000000000')).toFixed(2)}&nbsp;
                      </Typography>
                      <IconButton component={Link} to={`/proposal/${proposal.proposalId}`}>
                        <InfoIcon />
                      </IconButton>
                    </Paper>
                  </Grid>
                ))}
          </Grid>
        </main>
      </div>
    );
  }
}

ProposalCards.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(ProposalCards);
