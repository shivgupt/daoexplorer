import Divider from '@material-ui/core/Divider';
import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import PropTypes from 'prop-types';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// Material UI icons
import Active from '@material-ui/icons/DirectionsRun';
import Anonymous from '@material-ui/icons/VisibilityOff';
import Dormant from '@material-ui/icons/Snooze';
import Known from '@material-ui/icons/HowToRegTwoTone';
import NoRep from '@material-ui/icons/PowerOff';
import Rep from '@material-ui/icons/Power';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    width: 250,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
  listIcon: {
    marginRight: 1,
    marginLeft: 1,
  },
  sidebarText: {
    fontSize: 11,
  },
  padding: {
    padding: 0,
  },
  dense: {
    padding: 0,
  },
});

class UserFilters extends React.Component {
  render() {
  const {
    handleChange,
    classes,
    active,
    anonymous,
    dormant,
    noRep,
    repHolder,
    socialProfiles
  } = this.props;
    return (
      <List dense={true}>
        <ListItem dense={true} key={"repHolder"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={repHolder}
              onChange={handleChange('repHolder')}
              checkedIcon={<Rep/>}
              icon={<Rep/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Rep-Holders</Typography>} />
        </ListItem>
        <ListItem dense={true} key={"noRep"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={noRep}
              onChange={handleChange('noRep')}
              checkedIcon={<NoRep/>}
              icon={<NoRep/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>No-Rep</Typography>} />
        </ListItem>
        <Divider />
        <ListItem dense={true} key={"anonymous"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={anonymous}
              onChange={handleChange('anonymous')}
              checkedIcon={<Anonymous/>}
              icon={<Anonymous/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Anonymous</Typography>} />
        </ListItem>
        <ListItem dense={true} key={"socialProfiles"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={socialProfiles}
              onChange={handleChange('socialProfiles')}
              checkedIcon={<Known/>}
              icon={<Known/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Social-Profiles</Typography>} />
        </ListItem>
        <Divider />
        <ListItem dense={true} key={"dormant"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={dormant}
              onChange={handleChange('dormant')}
              checkedIcon={<Dormant/>}
              icon={<Dormant/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Dormant</Typography>} />
        </ListItem>
        <ListItem dense={true} key={"active"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={active}
              onChange={handleChange('active')}
              checkedIcon={<Active/>}
              icon={<Active/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Active</Typography>} />
        </ListItem>
      </List>
    )

  }
}

UserFilters.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserFilters);
