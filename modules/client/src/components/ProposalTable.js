import React from 'react';
import Proposal from './Proposal';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tableHeader: {
    backgroundColor: theme.palette.common.black,
    fontSize: 10,
    color: 'white',
  },
  tableContent: {
    padding: theme.spacing.unit * 2,
    fontSize: 8,
  },
});

class ProposalTable extends React.Component {
  state = {
    spacing: '16',
  };

  render(){
    const { spacing } = this.state;
    const { classes } = this.props;
    const { alchemyData, viewProposal } = this.props
    if (!alchemyData) return null
    //console.log(`alchemyData = ${JSON.stringify(this.props.alchemyData, null, 2)}`)
    return(
      <Grid id="ProposalTable" container className={classes.root} spacing={16}>
        <Grid item xs={7} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Proposals
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Status
          </Typography>
        </Grid>
        <Grid item xs={3} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            More
          </Typography>
        </Grid>
        {this.props.rowdata.map( proposal => (
          <Proposal proposal={proposal} viewProposal={viewProposal} alchemyData={alchemyData} />
        ))}
      </Grid>
    )
  }
}

ProposalTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProposalTable);
