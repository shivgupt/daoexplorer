import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
    textAlign: 'center',
  },
}))(TableCell);

class User extends React.Component {
  render() {
    const { viewUser, user } = this.props
    return (
      <TableRow key={user.address}>
        <CustomTableCell>
          {user.address}
        </CustomTableCell>
        <CustomTableCell>
          <IconButton onClick={() => viewUser(user.address)}>
            <InfoIcon />
          </IconButton>
        </CustomTableCell>
      </TableRow>
    )
  }
}

export { User }

