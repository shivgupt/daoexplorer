import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import { Link } from "react-router-dom";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tableHeader: {
    backgroundColor: theme.palette.common.black,
    fontSize: 10,
    color: 'white',
  },
  tableContent: {
    padding: theme.spacing.unit * 2,
    fontSize: 8,
  },
});

class VoteTable extends React.Component {
  state = {
    spacing: '16',
  };

  filterProposal(proposal) {
  const { viewProposal, alchemyData } = this.props
  if (alchemyData[proposal.proposalId])
    return true
  else
    return false
  }
  render() {
    const { spacing } = this.state;
    const { classes } = this.props;
    const { votes, viewProposal, alchemyData } = this.props
    //console.log(JSON.stringify(vote, null, 2))
    if (!votes) return null
    return (
      <Grid id="VoteTable" container className={classes.root} spacing={16}>
        <Grid item xs={7} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Proposal
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Vote
          </Typography>
        </Grid>
        <Grid item xs={3} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            More
          </Typography>
        </Grid>
        {
          votes.map( vote => (
            alchemyData[vote.proposal.proposalId] ? 
              <Grid key={vote.proposal.proposalId} container className={classes.root} spacing={16}>
                <Grid item xs={7}>
                  <Typography gutterBottom className={classes.tableContent}>
                    <a href={alchemyData[vote.proposal.proposalId].description}>{alchemyData[vote.proposal.proposalId].title}</a>
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography gutterBottom className={classes.tableContent}>
                    {vote.voteOption === "1" ? 'For' : 'Against'}
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <IconButton component={Link} to={`/proposal/${vote.proposal.proposalId}`} onClick={() => viewProposal(vote.proposal.proposalId)}>
                    <InfoIcon />
                  </IconButton>
                </Grid>
              </Grid>
            : null
          ))
        }
      </Grid>
    )
  }
}

VoteTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VoteTable);
