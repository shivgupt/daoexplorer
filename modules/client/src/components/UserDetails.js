import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import ProposalTable from './ProposalTable';
import { withStyles } from '@material-ui/core/styles';
import StakeTable from './StakeTable'
import VoteTable from './VoteTable'
import { Proposal } from './Proposal'
import ReputationGraph from './ReputationGraph'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 350,
    width: 250,
  },
  text: {
    fontSize: 8,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});

class UserDetails extends React.Component {
  state = {
    spacing: '16',
  };

  filterProposal(proposal) {
  const { viewProposal, alchemyData } = this.props
  if (alchemyData[proposal.proposalId])
    return true
  else
    return false
  }

  render() {
    const { user, viewProposal, alchemyData, daoRepChanges, accountRepChanges } = this.props
    const { classes } = this.props;
    const { spacing } = this.state;

    if (!alchemyData || !accountRepChanges || !daoRepChanges || !user) return <h2> Loading... </h2>

    return (
      <Grid container className={classes.root} spacing={16}>
        <Grid item xs={12}>
          <Typography gutterBottom>
            Reputation Graph
          </Typography>
        </Grid>
        <Grid item justify="center" xs={12}>
          <ReputationGraph daoRepChanges={daoRepChanges} accountRepChanges={accountRepChanges} />
        </Grid>
        <Grid item xs={12}>
          <Typography gutterBottom>
            User: {user.address}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.text}>
            Proposed
          </Typography>
        </Grid>
        <ProposalTable rowdata={user.proposals} viewProposal={viewProposal} alchemyData={alchemyData} />
        <Grid item xs={12}>
          <Typography className={classes.control}>
            Staked On
          </Typography>
        </Grid>
        <StakeTable stakes={user.stakes} viewProposal={viewProposal} alchemyData={alchemyData} />
        <Grid item xs={12}>
          <Typography className={classes.control}>
            Voted On
          </Typography>
        </Grid>
        <VoteTable votes={user.votes} viewProposal={viewProposal} alchemyData={alchemyData} />
      </Grid>
    )
  }
}

UserDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserDetails);
