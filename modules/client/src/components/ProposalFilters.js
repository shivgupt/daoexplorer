import Divider from '@material-ui/core/Divider';
import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import PropTypes from 'prop-types';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// Material UI icons
import Awaiting from '@material-ui/icons/HourglassFullOutlined';
import Boosted from '@material-ui/icons/OfflineBoltOutlined';
import Failed from '@material-ui/icons/HighlightOffOutlined';
import Nonboosted from '@material-ui/icons/BatteryAlertOutlined';
import Passed from '@material-ui/icons/CheckCircleOutline';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    width: 250,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
  listIcon: {
    marginRight: 1,
    marginLeft: 1,
  },
  sidebarText: {
    fontSize: 11,
  },
  padding: {
    padding: 0,
  },
  dense: {
    padding: 0,
  },
});

class ProposalFilters extends React.Component {
  render() {
  const { handleChange, classes, nonboosted, boosted, passed, failed, awaiting } = this.props;
    return (
      <List dense={true}>
        <ListItem id="filter_passed" dense={true} key={"passed"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={passed}
              onChange={handleChange('passed')}
              checkedIcon={<Passed/>}
              icon={<Passed/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Passed</Typography>} />
        </ListItem>
        <ListItem id="filter_failed" dense={true} key={"failed"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={failed}
              onChange={handleChange('failed')}
              checkedIcon={<Failed/>}
              icon={<Failed/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Failed</Typography>} />
        </ListItem>
        <ListItem id="filter_awaiting" dense={true} key={"awaiting"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={awaiting}
              onChange={handleChange('awaiting')}
              checkedIcon={<Awaiting/>}
              icon={<Awaiting/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Awaiting</Typography>} />
        </ListItem>
        <Divider />
        <ListItem id="filter_boosted" dense={true} key={"boosted"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={boosted}
              onChange={handleChange('boosted')}
              checkedIcon={<Boosted/>}
              icon={<Boosted/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Boosted</Typography>} />
        </ListItem>
        <ListItem id="filter_nonboosted" dense={true} key={"nonboosted"} className={classes.dense}>
          <ListItemIcon className={classes.listIcon}>
            <Switch
              checked={nonboosted}
              onChange={handleChange('nonboosted')}
              checkedIcon={<Nonboosted/>}
              icon={<Nonboosted/>}
            />
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Non-Boosted</Typography>} />
        </ListItem>
      </List>
    )

  }
}

ProposalFilters.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProposalFilters);
