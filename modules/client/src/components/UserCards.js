import _ from 'lodash';
import BN from 'bignumber.js';
import q from '../Query';
import React from 'react';
import ReputationGraph from './ReputationGraph'
import utils from '../Utils';

import UserFilters from './UserFilters';
import UserSort from './UserSort';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// Material UI Core
import AppBar from '@material-ui/core/AppBar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import green from '@material-ui/core/colors/green';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import red from '@material-ui/core/colors/red';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

// Material UI icons
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Docs from '@material-ui/icons/Assignment';
import InfoIcon from '@material-ui/icons/Info';
import ThumbDown from '@material-ui/icons/ThumbDownAlt';
import ThumbUp from '@material-ui/icons/ThumbUpAlt';
import TrendingUp from '@material-ui/icons/TrendingUp';
import TrendingDown from '@material-ui/icons/TrendingDown';

const avatar = "0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"

const drawerWidth = 150;

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  colorFor: {
    color: green[400]
  },
  colorAgainst: {
    color: red[700]
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  paper: {
    width: 250,
  },
  toolbar: {
    marginTop: 56,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
});

class UserCards extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleSortChange = this.handleSortChange.bind(this)
  }
  state = {
    open: false,
    sortOption: 'time',
    spacing: '8',
    active: true,
    anonymous: false,
    dormant: false,
    noRep: false,
    repHolder: true,
    socialProfiles: true,
    nonboosted: false,
  };

  handleDrawerOpen = () => {
    this.setState({open: true});
  }

  handleDrawer = () => {
    const { open } = this.state
    console.log(`prev = ${open}`)
    this.setState({open: !open});
  }

  handleSortChange = (event, value) => {
    console.log(`event value ${event.target.value}`)
    console.log(`value = ${value}`)
    this.setState({sortOption: event.target.value})
  }

  handleChange = key => (event, value) => {
    this.setState({
      [key]: value,
    });
  };

  async getUserData(user) {
    let result = await utils.execute(q.getAllReputationChangeForAccount(user, avatar), {});
    let accountRepChanges =  result.reputationChanges;
    return accountRepChanges
  }

  getFormatedDate(ts) {
    let date = new Date(ts)
    return date.toString().substring(0,15)
  }

  sort(users) {
    const { sortOption } = this.state;
    if (sortOption === 'time')
      return _.orderBy(
        users,
        [function(user) { return user.lastActive ? user.lastActive : 0 }],
        ['desc']
      )
    if (sortOption === 'numStake')
      return _.orderBy(
        users,
        [function(user) {
          return user.stakes ? user.stakes.length : 0
        }],
        ['desc']
      )
    if (sortOption === 'numVote')
      return _.orderBy(
        users,
        [function(user) { return user.votes ? user.votes.length : 0 }],
        ['desc']
      )
    if (sortOption === 'numProp')
      return _.orderBy(
        users,
        [function(user) { return user.proposals ? user.proposals.length : 0 }],
        ['desc']
      )
    if (sortOption === 'rep')
      return _.orderBy(
        users,
        [function(user) { return user.reputation ? (new BN(user.reputation).div(new BN('1000000000000000000'))).toNumber() : 0 }],
        ['desc']
      )
  }

  filterUsers(users) {
    const {
      active,
      anonymous,
      dormant,
      noRep,
      repHolder,
      socialProfiles
    } = this.state;
    const { alchemyData, accounts } = this.props
    return _.filter(users, function(user) {
      //if (!alchemyData[proposal.proposalId]) return false
      if ((!active && user.lastActive) ||
          (!dormant && !user.lastActive) ||
          (!repHolder && (new BN(user.reputation)).div(new BN('1000000000000000000')).toNumber() > 0) ||
          (!noRep && (new BN(user.reputation)).div(new BN('1000000000000000000')).toNumber() === 0) ||
          (!anonymous && !accounts[user.address]) ||
          (!socialProfiles && accounts[user.address])
      ) return false
      return true
    })
  }

  render() {
    const { spacing } = this.state;
    const {
      accounts,
      alchemyData,
      classes,
      daoRepChanges,
      proposal,
      theme,
      users,
      viewProposal,
      viewUser,
    } = this.props
    let filtered = this.filterUsers(users)
    let sorted = this.sort(filtered)

    if (!alchemyData) return null
    console.log(`users Data = ${JSON.stringify(this.props.users, null, 2)}`)
    console.log(`accounts Data = ${JSON.stringify(this.props.accounts, null, 2)}`)

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          color="default"
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: this.state.open,
          })}
        >
        </AppBar>

        <Drawer
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open,
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            }),
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawer}>
              {this.state.open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          <Divider />
          Sort By
          <Divider />
          <UserSort
            handleSortChange={this.handleSortChange}
            sortOption={this.state.sortOption}
          />
          <Divider />
          Filters
          <Divider />
          <UserFilters
            handleChange={this.handleChange}
            repHolder={this.state.repHolder}
            noRep={this.state.noRep}
            anonymous={this.state.anonymous}
            lastActiveDate={this.state.lastActiveDate}
            socialProfiles={this.state.socialProfiles}
            dormant={this.state.dormant}
            active={this.state.active}
          />
        </Drawer>

        <main className={classes.content}>
          <div />
          <Grid container className={classes.demo} justify="center" spacing={Number(spacing)}>
            {sorted.map( user => (
                <Grid key={user.address} item>
                  <Card className={classes.paper}>
                    <CardContent>
                      <Link to={`/user/${user.address}`}>{accounts[user.address] ? accounts[user.address].name : 'Anonymous'}</Link>
                      <Typography style={{fontSize: 8}}>{user.address}</Typography>
                      <br/>
                      <br/>
                      <strong>Rep:</strong> {(new BN(user.reputation)).div(new BN('1000000000000000000')).toFixed(2)}
                      <br/>
                      <strong>Last Seen:</strong> {user.lastActive ? this.getFormatedDate(user.lastActive * 1000) : "Dormant"}
                      <br/>
                      <br/>
                      <Docs color="secondary"/>
                      <strong>Proposed:</strong> {user.proposals ? user.proposals.length : 0}
                      <br/>
                      <TrendingUp className={classes.colorFor}/>
                      <TrendingDown className={classes.colorAgainst}/>
                      <strong>Staked:</strong> {user.stakes ? user.stakes.length : 0}
                      <br/>
                      <ThumbUp className={classes.colorFor}/>
                      <ThumbDown className={classes.colorAgainst}/>
                      <strong>Voted:</strong> {user.votes ? user.votes.length : 0}
                      <br/>
                      <IconButton component={Link} to={`/user/${user.address}`}>
                        <InfoIcon />
                      </IconButton>
                    </CardContent>
                  </Card>
                </Grid>
            ))}
          </Grid>
        </main>
      </div>
    );
  }
}

UserCards.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserCards);

