import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tableHeader: {
    backgroundColor: theme.palette.common.black,
    fontSize: 10,
    color: 'white',
  },
  tableContent: {
    padding: theme.spacing.unit * 2,
    fontSize: 8,
  },
});

class VoterTable extends React.Component {
  state = {
    spacing: '16',
  };

  render() {
    const { spacing } = this.state;
    const { classes } = this.props;
    const { votes, viewUser, alchemyData } = this.props
    if (!votes) return null
    return (
      <Grid id='VoterTable' container className={classes.root} spacing={16}>
        <Grid item xs={7} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Voter
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Vote
          </Typography>
        </Grid>
        <Grid item xs={3} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            More
          </Typography>
        </Grid>
        {
          votes.map( vote => (
          <Grid key={vote.voter.address} container className={classes.root} spacing={16}>
            <Grid item xs={7}>
              <Typography gutterBottom className={classes.tableContent}>
                {vote.voter.address}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography gutterBottom className={classes.tableContent}>
                {vote.voteOption === "1" ? 'For' : 'Against'}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <IconButton onClick={() => viewUser(vote.voter.address)}>
                <InfoIcon />
              </IconButton>
            </Grid>
          </Grid>
          ))
        }
      </Grid>
    )
  }
}

VoterTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VoterTable);
