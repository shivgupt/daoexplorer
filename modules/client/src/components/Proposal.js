import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import { Link } from "react-router-dom";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tableHeader: {
    backgroundColor: theme.palette.common.black,
    fontSize: 10,
    color: 'white',
  },
  tableContent: {
    padding: theme.spacing.unit * 2,
    fontSize: 8,
  },
});

class Proposal extends React.Component {
  state = {
    spacing: '16',
  };

  render() {
    const { spacing } = this.state;
    const { classes } = this.props;
    const { proposal, alchemyData, viewProposal } = this.props
    //console.log(`proposal = ${JSON.stringify(this.props.proposal)}`)
    if ( !alchemyData[proposal.proposalId] ) return null

    return (
      <Grid key={proposal.proposalId} container className={classes.root} spacing={16}>
        <Grid item xs={7}>
          <Typography gutterBottom className={classes.tableContent}>
            <a href={alchemyData[proposal.proposalId].description}>{alchemyData[proposal.proposalId].title}</a>
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography gutterBottom className={classes.tableContent}>
            {proposal.decision === "1" ? <p> Passed </p> : (proposal.decision === "2" ? <p> Failed </p> : <p> Awaiting </p>)}
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <IconButton component={Link} to={`/proposal/${proposal.proposalId}`}>
            <InfoIcon />
          </IconButton>
        </Grid>
      </Grid>
    )
  }
}

Proposal.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Proposal);
