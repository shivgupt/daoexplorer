import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import BN from 'bignumber.js';
import { Link } from "react-router-dom";

const normalize = new BN('1000000000000000000')

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tableHeader: {
    backgroundColor: theme.palette.common.black,
    fontSize: 12,
    color: 'white',
  },
  tableContent: {
    padding: theme.spacing.unit * 2,
    fontSize: 12,
  },
});

class StakeTable extends React.Component {
  state = {
    spacing: '16',
  };

  render() {
    const { stakes, viewProposal, alchemyData } = this.props
    const { spacing } = this.state;
    const { classes } = this.props;
    if (!stakes) {
      return null
    }
    return (
      <Grid id="StakeTable" container className={classes.root} spacing={16}>
        <Grid item xs={6} className={classes.tableHeader}>
          <Typography gutterBottom noWrap className={classes.tableHeader}>
            Proposal
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Amount
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            Prediction
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.tableHeader}>
          <Typography gutterBottom className={classes.tableHeader}>
            More
          </Typography>
        </Grid>
        {
          stakes.map( stake => (
            alchemyData[stake.proposal.proposalId] ?
              <Grid key={stake.proposal.proposalId} container className={classes.root} spacing={16}>
                <Grid item xs={6}>
                  <Typography gutterBottom className={classes.tableContent}>
                      <a href={alchemyData[stake.proposal.proposalId].description}>{alchemyData[stake.proposal.proposalId].title}</a>
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography gutterBottom className={classes.tableContent}>
                    {new BN(stake.stakeAmount).dividedBy(normalize).toString()}
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography gutterBottom className={classes.tableContent}>
                    {stake.prediction === "1" ? 'For' : 'Against'}
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <IconButton component={Link} to={`/proposal/${stake.proposal.proposalId}`}>
                    <InfoIcon />
                  </IconButton>
                </Grid>
              </Grid>
            : null
          ))
        }
      </Grid>
    )
  }
}

StakeTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StakeTable);
