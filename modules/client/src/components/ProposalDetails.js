import React from 'react';
//import { StakerTable } from './StakerTable'
//import { VoterTable } from './VoterTable'
import { Link } from "react-router-dom";

// Material UI integration
import VoterTable from './VoterTable'
import StakerTable from './StakerTable'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 350,
    width: 250,
  },
  text: {
    fontSize: 8,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});

class ProposalDetails extends React.Component {
  state = {
    spacing: '16',
  };

  render() {
    const { proposal, viewProposal, viewUser, alchemyData } = this.props
    const { classes } = this.props;
    const { spacing } = this.state;
    if (!alchemyData || !proposal) return <h2> Loading... </h2>
    console.log(JSON.stringify(proposal, null, 2))
    return (
      <Grid container className={classes.root} spacing={16}>
        <Grid item xs={12}>
          <Typography gutterBottom>
            Proposal: 
            {alchemyData[proposal.proposalId] ? 
              <a href={alchemyData[proposal.proposalId].description}>{alchemyData[proposal.proposalId].title}</a>
              : <a href="#"> No Title </a>
            }
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.text}>
            Created By: <Link to={`/user/${proposal.proposer.address}`}>{proposal.proposer.address}</Link>
            <br />
            Decision: {proposal.decision === "1" ? 'Passed' : (proposal.decision === "2" ? 'Failed' : 'Awaiting')}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.control}>
            Stakes
          </Typography>
        </Grid>
        <StakerTable stakes={proposal.stakes} viewUser={viewUser} viewProposal={viewProposal} alchemyData={alchemyData} />
        <Grid item xs={12}>
          <Typography className={classes.control}>
            Votes
          </Typography>
        </Grid>
        <VoterTable votes={proposal.votes} viewUser={viewUser} viewProposal={viewProposal} alchemyData={alchemyData} />
      </Grid>
    )
  }
}

ProposalDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProposalDetails);
