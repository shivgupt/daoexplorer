import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// Material UI icons
import Boosted from '@material-ui/icons/OfflineBoltOutlined';
import Payout from '@material-ui/icons/AttachMoneyRounded';
import Star from '@material-ui/icons/Star';
import Time from '@material-ui/icons/AccessTimeRounded';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  listIcon: {
    marginRight: 8,
    marginLeft: 1,
  },
  sidebarText: {
    fontSize: 11,
  },
  padding: {
    padding: 0,
  },
});


class ProposalSort extends React.Component {
  render() {
  const { handleSortChange, classes, sortOption } = this.props;
    return (
      <List dense={true}>
        <ListItem id="sort_date" key={"Date"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Time/>} icon={<Time/>} value="time" onChange={handleSortChange} checked={sortOption === 'time'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Date</Typography>} />
        </ListItem>
        <ListItem id="sort_eth" key={"Eth Payout"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Payout/>} icon={<Payout/>} value="eth" onChange={handleSortChange} checked={sortOption === 'eth'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Eth Payout</Typography>} />
        </ListItem>
        <ListItem id="sort_stake" key={"Stake"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Boosted/>} icon={<Boosted/>} value="stake" onChange={handleSortChange} checked={sortOption === 'stake'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Stake</Typography>} />
        </ListItem>
        <ListItem id="sort_rep" key={"Rep Payout"}>
          <ListItemIcon className={classes.listIcon}>
            <Radio className={classes.padding} color="primary" checkedIcon={<Star/>} icon={<Star/>} value="rep" onChange={handleSortChange} checked={sortOption === 'rep'}/>
          </ListItemIcon>
          <ListItemText primary={<Typography className={classes.sidebarText}>Rep Payout</Typography>} />
        </ListItem>
      </List>
    )
  }
}

ProposalSort.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProposalSort);
