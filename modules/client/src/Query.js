import Buffer from 'buffer';
import { ethers } from 'ethers';

let stakes = `
stakes {
  staker {
    address
  }
  stakeAmount
  prediction
}
`

let votes = `
votes {
  voter {
    address
  }
  voteOption
}
`

let redemptions = `
redemptions {
  proposal {
    proposalId
  }
  account {
    address
  }
  type
  amount
}
`

let common = `
  proposalId
  proposer {
    address
  }
  dao {
    avatarAddress
  }
  state
  decision
`
const q = {}

q.getAllProposals = () => {
  return `
    query { 
      proposals { 
        proposalId
        decision 
      }
    }
  `
}

q.getAllProposalsWithDetails = () => {
  return `
    query {
      proposals {
        ${common}
        ${stakes}
        ${votes}
        ${redemptions}
      }
    }
  `
}

q.getAllProposalsFor = (avatar) => {
  return `
    query {
        proposals (
          where: {
            dao: "${avatar}"
          },
          orderBy: submittedTime,
          orderDirection: desc
        ) {
          proposalId
          decision
          ${stakes}
          submittedTime
          executionTime
          totalStakeFor
          totalStakeAgainst
          totalRepFor
          totalRepAgainst
        }
        crproposals
        {
          proposalId
          ethReward
          reputationChange
        }
      }
  `
}

q.getAllProposalsWithDetailsFor = (avatar) => {
  return `
    query {
      dao (id: "${avatar}") {
        proposals {
          ${common}
          ${stakes}
          ${votes}
          ${redemptions}
        }
      }
    }
  `
}

q.getAllProposalsProposedBy = (proposer) => {
  return `
    query {
      accounts ( where: { address: "${proposer}" }) {
        proposals {
          ${common}
          ${stakes}
          ${votes}
          ${redemptions}
        }
      }
    }
  `
}

q.getAllProposalsVotedBy = (voter) => {
  return `
    query {
      accounts ( where: { address: "${voter}" } )
      votes {
        proposal {
          ${common}
        }
      }
    }
  `
}

q.getAllProposalsStakedBy = (staker) => {
  return `
    query {
      accounts ( where: { address: "${staker}" } )
      stakes {
        proposal {
          ${common}
        }
      }
    }
  `
}

q.getDetailsOfProposal = (id) => {
  return `
    query {
      proposal( id: "${id}" ) {
        ${common}
        ${stakes}
        ${votes}
        ${redemptions}
      }
    }
  `
}

q.getAllRewardsFor = (beneficiary) => {
  return `
    query {
      accounts ( where: { address: "${beneficiary}" }) {
        ${redemptions}
      }
    }
  `
}

q.getAllUsers = (avatar) => {
  return `
    query {
      dao ( id: "${avatar}" ) {
        accounts {
          address
          lastActive
          reputation
          stakes {
            time
          }
          votes {
            time
          }
          proposals {
            submittedTime
          }
        }
      }
    }
  `
}

q.getAllDetailsForUser = (user, avatar) => {
  const accountId = ethers.utils.keccak256(user + avatar.substring(2))
  return `
    query {
      account ( id: "${accountId}" ) {
        address
        lastActive
        proposals {
          proposalId
          decision
        }
        stakes {
          prediction
          stakeAmount
          proposal {
            proposalId
          }
        }
        votes {
          voteOption
          proposal {
            proposalId
          }
        }
      }
    }
  `
}

q.getAllReputationChangeForDao = (avatar) => {
  return `
    query {
      reputationChanges ( where: {
        dao: "${avatar}"
      },
        orderBy: time,
        orderDirection: asc
      ) {
          amount
          time
          totalSupplyAfter
      }
    }
  `
}

q.getAllReputationChangeForAccount = (user, avatar) => {
  const accountId = ethers.utils.keccak256(user + avatar.substring(2))
  return `
    query {
      reputationChanges ( where: {
        account: "${accountId}",
      },
        orderBy: time,
        orderDirection: asc
      ) {
          amount
          time
          totalSupplyAfter
      }
    }
  `
}

export default q
