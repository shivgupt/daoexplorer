import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { unregister } from './registerServiceWorker';
import { Provider } from "react-redux";
import store from "./store/index";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

unregister();

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById('root')
);
