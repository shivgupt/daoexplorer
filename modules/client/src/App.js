import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import './App.css';
import q from './Query';
import BN from 'bignumber.js';
//import { ProposalDetails } from './components/ProposalDetails';
import { ProposalTable } from './components/ProposalTable';
//import { UserDetails } from './components/UserDetails';
import { UserTable } from './components/UserTable';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// Material UI
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import ProposalCards from './components/ProposalCards';
import PropTypes from 'prop-types';
import NavBar from './components/NavBar';
import Switch from '@material-ui/core/Switch';
import UserCards from './components/UserCards';
import ProposalDetails from './components/ProposalDetails';
import UserDetails from './components/UserDetails';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const avatar = "0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"
const proposals_url = 'https://daostack-alchemy.herokuapp.com/api/proposals'
const accounts_url = 'https://daostack-alchemy.herokuapp.com/api/accounts'

//const graph_url = 'https://staging.bohendo.com/api/graph';
const graph_url = 'https://dao-explorer.com/api/graph';
//const graph_url = '/api/graph'

const styles = theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
})

class App extends Component {
  constructor(props) {
  super(props)
  this.viewHome = this.viewHome.bind(this)
  this.viewProposal = this.viewProposal.bind(this)
  this.viewProposals = this.viewProposals.bind(this)
  this.viewUser = this.viewUser.bind(this)
  this.viewUsers = this.viewUsers.bind(this)
  this.state = {
      accounts: null,
      accountRepChanges: null,
      alchemyData: null,
      checked: true,
      daoRepChanges: null,
      error: null,
      loading: true,
      mode: "Welcome",
      proposal: null,
      proposals: null,
      type: "",
      user: null,
      users: null,
    }
  }

  async execute (query, variables) {
    try{
      const response = await axios.post(graph_url,
        {
          query,
          variables,
          crossdomain: true,
          headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          }
        },
      );
      //console.log(`executed query, got reponse: ${JSON.stringify(response, null, 2)}`);
      return response.data.data;
    } catch (error) { this.setState(() => ({ error })) }
  }

  async viewProposal(id) {
    console.log(`viewProposal(${id}) activated!`)
    const result = await this.execute(q.getDetailsOfProposal(id));
    //console.log(`viewProposal(${id}) = ${JSON.stringify(result, null, 2)}`)
    this.setState({
      mode: "ProposalDetails",
      proposal: result.proposal
    }, () => {
      console.log(`updated mode = ${this.state.mode}, update proposal = ${this.state.proposal}`)
    })
  }

  async viewProposals() {
    if (this.state.proposals) {
      this.setState({ mode : "Proposals" })
      return null;
    }
    const result = await this.execute(q.getAllProposalsFor(avatar), {});
    //console.log(`viewProposals result = ${JSON.stringify(result)}`)
    const crproposals = _.keyBy(result.crproposals, "proposalId" )
    _.forEach(result.proposals, function(proposal) {
      if (crproposals[proposal.proposalId]) {
        proposal.ethReward = new BN(crproposals[proposal.proposalId].ethReward).div(new BN('1000000000000000000')).toNumber()
        proposal.reputationChange = new BN(crproposals[proposal.proposalId].reputationChange).div(new BN('1000000000000000000')).toNumber()
      } else {
        proposal.ethReward = 0
        proposal.reputationChange = 0
      }
    })
    //console.log(JSON.stringify(result.proposals, null, 2))

    this.setState(() => ({
      mode: "Proposals",
      proposals: result.proposals
    }));
  }

  async viewUser(user) {
    console.log(`viewUser(${user}) activated!`)
    console.log(`User = ${user}`)
    let result = await this.execute(q.getAllDetailsForUser(user, avatar), {});
    const account = result.account;
    result = await this.execute(q.getAllReputationChangeForAccount(user, avatar), {});
    const reputationChanges = result.reputationChanges;

    //console.log(`reputation changes = ${JSON.stringify(result.reputationChanges, null, 2)}`);
    this.setState({
      mode: "UserDetails",
      user: account,
      accountRepChanges: reputationChanges
    })
  }

  async viewAdvanced() {
      console.log(`viewAdvanced`);
      window.location.href = '/graphiql/'
  }

  async viewHome() {
      this.setState({ mode : "Welcome" })
  }

  async viewUsers() {
    if (this.state.users) {
      this.setState({ mode : "Users" })
      return null;
    }

    const result = await this.execute(q.getAllUsers(avatar), {});
    //console.log(JSON.stringify(result, null, 2));

    this.setState(() => ({
      mode: "Users",
      users: result.dao.accounts
    }));
  }

  async componentDidMount() {
    this.setState(() => ({ loading: true }))
    try {
      let result = await axios.get(proposals_url);
      const alchemyData = _.keyBy(result.data, "arcId")

      result = await this.execute(q.getAllReputationChangeForDao(avatar), {});
      const daoRepChanges = result.reputationChanges;

      result = await axios.get(accounts_url);
      const accounts = _.keyBy(result.data, "ethereumAccountAddress")
      this.setState(() => ({
        accounts: accounts,
        alchemyData: alchemyData,
        daoRepChanges: daoRepChanges,
        loading: false
      }))
    } catch (error) {
      this.setState(() => ({error}))}
  }

  handleChange = () => (e) => {
    this.setState({checked: e.target.checked})
    console.log('change = '+ this.state.checked)
  }

  render() {
    const {classes, theme} = this.props
    let routeProps = {
      accounts: this.state.accounts,
      accountRepChanges: this.state.accountRepChanges,
      daoRepChanges: this.state.daoRepChanges,
      proposal: this.state.proposal,
      proposals: this.state.proposals,
      user: this.state.user,
      users: this.state.users,
      viewProposal: this.viewProposal,
      viewProposals: this.viewProposals,
      viewUser: this.viewUser,
      viewUsers: this.viewUsers,
      alchemyData: this.state.alchemyData,
    }

    return (
      <div className={App}>
        <CssBaseline/>
        <NavBar viewHome={this.viewHome} viewProposals={this.viewProposals} viewUsers={this.viewUsers}/>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Typography title align={"center"}>
            <Route exact path="/" render={() => <h2>Genesis Alpha</h2> } />
            <Route exact path="/home" render={() => <h2>Genesis Alpha</h2> } />
            <Route
              path="/users"
              render={() => {
                if (!routeProps.users) {
                  this.viewUsers();
                  routeProps.users = this.state.users;
                }
                if (this.state.loading) return <h2> Loading... </h2>
                return <UserCards {...routeProps} />}
              }
            />
            <Route
              path="/proposals"
              exact
              render={() => {
                if (!routeProps.proposals) {
                  this.viewProposals();
                  routeProps.proposals = this.state.proposals;
                }
                if (this.state.loading) return <h2> Loading... </h2>
                return <ProposalCards {...routeProps} />}
              }
            />
            <Route
              path="/user/:id"
              exact
              render={({match}) => {
                if (!routeProps.user) {
                  this.viewUser(match.params.id)
                }
                return <UserDetails {...routeProps}/>
              }}
            />
            <Route
              path="/proposal/:id"
              exact
              render={({match}) => {
                if (!routeProps.proposal) {
                  this.viewProposal(match.params.id)
                }
                return <ProposalDetails {...routeProps}/>
              }}
            />
          </Typography>
        </main>
      </div>
    );
  }
}

function home() {
  return (
  <div>
    <h2>home</h2>
  </div>
  );
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true })(App);
