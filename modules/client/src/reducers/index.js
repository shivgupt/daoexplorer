import { ADD_ARTICLE } from "../constants/action-types";

const initialState = {
  error: null,
  proposals: null,
  users: null,
  proposal: null,
  user: null,
  type: "",
  mode: "Welcome",
  alchemyData: null
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ARTICLE:
      return { ...state, articles: [...state.articles, action.payload] };
    default:
      return state;
  }
};

export default rootReducer;
