import q from './Query';
import axios from 'axios';

const avatar = "0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"
const graph_url = 'https://dao-explorer.com/api/graph';

const Utils = {}

Utils.execute =  async (query, variables) => {
    try{
      const response = await axios.post(graph_url,
        {
          query,
          variables,
          crossdomain: true,
          headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          }
        },
      );
      //console.log(`executed query, got reponse: ${JSON.stringify(response, null, 2)}`);
      return response.data.data;
    } catch (error) { this.setState(() => ({ error })) }
  }

export default Utils
