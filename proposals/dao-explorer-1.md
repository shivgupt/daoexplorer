[Alchemy Explorer - Phase 1]

#PROPOSAL DESCRIPTION
This proposal is aimed to fund the Phase-1 of the [Alchemy Explorer](https://gitlab.com/shivgupt/DAOstack-subgraph) - a dashboard to view stats of proposal/stake-holder/voter

## Token Requested

 - 200 Reputation: 
    - 150 : about 1.2% because I missed typed the amount in first reputation request
    - 50  : about 0.4% for this work

 - 6 ETH

## Description of the project
As a developer I was able to view the stats of my previous proposal (all the voters and stake-holders) by fetching for events from the blockchain but for a community member, with non tech background, Alchemy UI does not provide an easy way to get this information.

This project aims to build a dashboard which provides an easy UI to view stats of proposal/staker/voter. For example:
    - all proposals proposed by given proposer (ethAddress)
    - all proposals staked by given staker
    - all proposals voted by given voter

PS: These are just sample information and I would love to get input from other users and product leads on what stats they would like to see.

## About Proposer
My name is Shivani, Pollinator at DAOstack

You can find more about me in my [first proposal](https://docs.google.com/document/d/1MPxdWDSxF_ri6u948VvtY1r14kEIqI5aNHWHNq05_C0/edit) for reputation
Checkout my work at: [Github](https://github.com/shivgupt/), [Gitlab](https://gitlab.com/shivgupt/)

## How can proposer guarantee results?  
 - The proposer already has working results to fetch some of the information which was done as part of the research on feasibility of this project.

## How does the proposal promote the DAO’s objective?
 - Its important for all the community members to be able to get the general stats of the DAO which can in turn help members to make better decision. For example A voter can refer to the quality of work delivered by the proposer in past (if any) to decide whether to vote again or not. Similarly a voter who voted for passing a bad/good proposal in past can be easily recognized by the other members accordingly.

## Metrics for success
Phase 1 completion would aim at backend setup for the explorer and would involve following tasks to be complete
 - Get the information emitted from the blockchain and store it in a database using Graph Protocol
 - Provide an API so clients without a web3 connection can access the data
 - Be able to query the interesting information like all the stakers for a given proposal or all the proposals by a given eth address etc

## How do we report on project’s progress
 - The progress can be tracked on open git repository.
 - I would like to request 2-5 mins in general meeting on October 16, 2018 to present the working solution so far.

## Timeline
 - Phase 1: About 2 weeks
 - Phase 2: TBD, ( should take about 3-4 weeks but will describe in Phase 2 proposal)

## Future iterations
 - Phase 2: Visualization of the information in the form of graphs/pie-charts and frontend development
