# Add UI and new features to explorer

## Token Requested

 - 100 Reputation
 - 30 ETH: For ~80 hours of work split between Bo and Shiv

## TL;DR

This proposal is aimed to fund Phase-2 of the [Alchemy Explorer](https://gitlab.com/shivgupt/daoexplorer) which would focus on development of better UI and addition of some useful features requested by the community.

## Background

In the last two proposals we build a backend to serve data for the dao explorer which allowed users to view user history re proposals, votes, stakes made and Reputation change over time. Also to enable the features as requested/mentioned in proposal workshop.

## Proposal Overview

This proposal will fund us while we work on developing the features requested and stated most useful by fellow pollinators. Some bugs were discovered in our last deployment so we'll fix those while we're at it. We shall work on adding features in the following order of priority

 1. Material UI integration for better frontend
 2. Proposal Filtered by - status, Rep asked/turnout, date, Gen staked
 3. Proposal Sorted by - Rep turnout, Rep asked, Gen staked, Date
 4. User Filtered by - last active, Rep, Gen, number of proposals voted/created/staked
 5. User Sorted by - last active, Rep, Gen, number of proposals voted/created/staked
 6. Vote & stake functionality
 7. Proposal status and deliverable link update by proposer or ATF members

## Timeline

Days and duration we will be working:
  - Jan 7th - 14th: 40-60 hours of work
  - Jan 28th - Feb 1st: 20-40 hours of work

Expected time investment per feature:
  - 20-30 hrs: UI integration
  - 20-30 hrs: Proposals and User Profile filters and sorting
  - 20-40 hrs: Vote and stake functionality
  - 30-50 hrs: Proposal status and deliverable link update

We will provide an update on Jan 14th on the progress so far
