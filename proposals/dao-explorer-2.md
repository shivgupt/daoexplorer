# Upgrade Explorer Backend

## Token Requested
  - 0.5% Rep
  - 12 ETH for 60 - 80 hours of work

## About Me

[see here](https://docs.google.com/document/d/1MPxdWDSxF_ri6u948VvtY1r14kEIqI5aNHWHNq05_C0)

## TL;DR

Previous proposal gathered critical data from one contract w a simple UI. I built upon that work to track 3 additional contracts which allows to add support for extra features mentioned in future iterations section and I also worked with the DAOstack team to help them incorporate these changes into their goal of overhauling the alchemy caching layer.

## Background

In the last phase of the alchemy explorer, I created contract tracker for one contract: the Genesis Protocol. Tracking events from this one contract gave access to all new proposals, votes, and stakes. I also made super simple frontend for the user to explore this new data.

In the last proposal I asked for compensation for finishing the backend expecting it to be a week's worth of work because I expected that's how long it would take to finish gathering data. But an exhaustive backend that would also support the features discussed in pre-proposal workshop is taking a lot longer than expected. The fact that I am working with brand new tools that are lacking documentation makes the problem tricky. Luckily, I've been able to share a little of what I've learned with the DAOstack team to help them get up to speed with graph node.

## Proposal Overview

I upgraded the explorer backend to track 3 other contracts of alchemy and to support the features listed in future iteration section (fixing some bugs currently). This proposal is aimed to get the backend setup done at-least for the features discussed so far. It would include a simple UI to give a sneak-peak of some features from future iteration section (reputation of User over time ?).

**Extra Contracts**
 - DAO Creator: to track new daos that are created so the explorer could also be used for an overview of all daos, not just Genesis Alpha
 - ContribuitionRewards: to get data about payments made. Gives timestamps for when these payments were delivered. Also gives more info about this type of proposal like the beneficiary address (which can be different than proposer)
 - Reputation: to track reputation of user over time

This stuff was useful for the caching server layer DAOstack team is working on and I submitted a [pull request](https://github.com/daostack/subgraph/pull/3) with version 1 of mappings for the above contracts. The DAOstack core team has expanded upon these mappings and hopes to use it to replace Alchemy's caching server.

## Timeline

 - Oct 9th: Previous proposal was completed with basic UI.

 - Oct 23rd: Submitted a PR to the DAOstack team's subgraph repo to contribute the work I've done so far.

 - Nov 13th: I will give an update in the pollinator's meeting & expect the last bugs in this backend to be fixed. At this point, the backend should be able to support the features describe below

## Future iterations

Once the work for this proposal is done, I'll upgrade the simple frontend to add some more powerful features. 

Some of the features I'd like to add for phase 2 of explorer:
  - Alchemy stats page
    - History for passed/not-passed vs boosted/not-boosted
    - Active User categories in last day/week/month: i.e. %actProposers, %actUser, %actVoter
    - Proposals submitted in last day/week/month
    - Redemptions in last day/week/month
  - User stats page
    - User reputation graph over time
    - User last active on
    - Proposals by the user created/voted/staked in last day/week/month
    - Redemptions by the user in last day/week/month
    - Number of Votes/Stakes/Proposals made to date
