#!/usr/bin/env bash

project=daoexplorer

docker stack rm $project
echo -n "Waiting for the $project stack to shutdown."

while [[ -n "`docker container ls | tail -n +2 | grep $project`" ]]
do
    echo -n '.'
    sleep 2
done
echo ' Goodnight!'

