FROM node:10-alpine
WORKDIR /root
ENV HOME /root
RUN apk add --update --no-cache bash curl g++ gcc git jq libsecret-dev make python
RUN yarn global add webpack webpack-cli webpack-dev-server
COPY ops /ops
ENTRYPOINT ["bash", "/ops/permissions-fixer.sh"]
