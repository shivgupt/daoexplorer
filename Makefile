# get absolute paths to important dirs
cwd=$(shell pwd)
client=$(cwd)/modules/client
contracts=$(cwd)/modules/contracts
subgraph=$(cwd)/modules/subgraph

# Get the src ie prerequisites for each module
find_opts=-type f -not -path "*/node_modules/*" -not -name "*.swp"
client_src=$(shell find $(client)/src $(client)/ops $(find_opts))
contract_src=$(shell find $(contracts)/contracts $(contracts)/migrations $(contracts)/ops $(find_opts))
subgraph_src=$(shell find $(subgraph)/src $(subgraph)/ops $(find_opts))
proxy_src=$(shell find modules/proxy $(find_opts))

# Define docker image names
project=daoexplorer
client_image=$(project)_client
ethprovider_image=$(project)_ethprovider
graph_image=$(project)_graph
proxy_image=$(project)_proxy

# Setup docker run time
# If on Linux, give the container our uid & gid so we know what to set permissions to
# On Mac the VM docker runs in takes care of this for us so don't pass in an id
id=$(shell id -u):$(shell id -g)
run_as_user=$(shell if [[ "`uname`" == "Darwin" ]]; then echo "--user $(id)"; fi)
docker_run=docker run --name=$(project)_buidler --tty --rm $(run_as_user)
docker_run_in_client=$(docker_run) --volume=$(client):/root $(project)_builder:dev $(id)
docker_run_in_contracts=$(docker_run) --volume=$(contracts):/root $(project)_builder:dev $(id)
docker_run_in_subgraph=$(docker_run) --volume=$(subgraph):/root	--volume=$(contracts)/build:/contracts/build --network=ipfs $(project)_builder:dev $(id)

# Use the version number from package.json to tag prod docker images
version=$(shell cat package.json | grep "\"version\":" | awk -F '"' '{print $$4}')
net_version=4447

# Special variable: tells make here we should look for prerequisites
SHELL=/bin/bash
VPATH=ops:build:$(client)/build:$(contracts)/build:$(subgraph)/build

# Shortcuts for various executables
webpack=./node_modules/.bin/webpack
graph_cli=./node_modules/.bin/graph
truffle=./node_modules/.bin/truffle
ipfs_start=docker run --name ipfs --network=ipfs --rm --detach --publish=5001:5001 ipfs/go-ipfs 2> /dev/null || true
ipfs_stop=docker container stop ipfs 2> /dev/null || true

log_start=@echo "=============";echo "[Makefile] => Start building $@"; date "+%s" > build/.timestamp
log_finish=@echo "[Makefile] => Finished building $@ in $$((`date "+%s"` - `cat build/.timestamp`)) seconds";echo "=============";echo

# Env setup
me=$(shell whoami)
$(shell mkdir -p build $(client)/build $(contracts)/build $(subgraph)/build)
$(shell docker network create ipfs 2> /dev/null)

####################
# Begin phony rules
.PHONY: default all dev prod clean stop purge deploy deploy-live

default: dev
all: dev prod
dev: proxy ethprovider graph clean-node-modules
prod: proxy-prod graph-prod

stop:
	docker container stop $(project)_buidler 2> /dev/null || true
	docker container stop ipfs 2> /dev/null || true
	bash ops/stop.sh
	docker container prune --force

clean:
	rm -rf build/*
	rm -rf $(client)/build/*
	rm -rf $(subgraph)/build/*

purge: clean stop
	rm -rf $(contracts)/build/*
	rm -f $(contracts)/ops/ganache.log
	docker container prune --force
	docker volume rm `docker volume ls -q | grep "[0-9a-f]\{64\}" | tr '\n' ' '` 2> /dev/null || true
	docker volume rm --force daoexplorer_graph_dev daoexplorer_chain_dev 2> /dev/null

deploy: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/daoexplorer/proxy:latest
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/daoexplorer/graph:latest
	docker push registry.gitlab.com/$(me)/daoexplorer/proxy:latest
	docker push registry.gitlab.com/$(me)/daoexplorer/graph:latest

deploy-live: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/daoexplorer/proxy:$(version)
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/daoexplorer/graph:$(version)
	docker push registry.gitlab.com/$(me)/daoexplorer/proxy:$(version)
	docker push registry.gitlab.com/$(me)/daoexplorer/graph:$(version)

####################
# Begin Real Rules

# Proxy

proxy-prod: $(proxy_src) explorer.js
	$(log_start)
	docker build --file modules/proxy/prod.dockerfile --tag $(proxy_image):latest .
	$(log_finish) && touch build/proxy-prod

proxy: $(proxy_src)
	$(log_start)
	docker build --file modules/proxy/dev.dockerfile --tag $(proxy_image):dev .
	$(log_finish) && touch build/proxy

# Client

explorer.js: client-node-modules $(client_src) $(client)/ops/dockerfile $(client)/ops/webpack.config.dev.js
	$(log_start)
	$(docker_run_in_client) "node ops/build.js"
	$(log_finish) && touch build/explorer.js

client-node-modules: builder $(client)/package.json
	$(log_start)
	$(docker_run_in_client) "yarn install"
	$(log_finish) && touch build/client-node-modules

# Graph node & subgraph

graph: subgraph $(subgraph)/ops/entry.sh
	$(log_start)
	docker build --file $(subgraph)/ops/graph.dockerfile --tag $(graph_image):dev $(subgraph)
	$(log_finish) && touch build/graph

graph-prod: subgraph-prod $(subgraph)/ops/entry.sh
	$(log_start)
	docker build --file $(subgraph)/ops/graph.dockerfile --tag $(graph_image):latest $(subgraph)
	$(log_finish) && touch build/graph-prod

subgraph: sugraph-node-modules contract-artifacts $(subgraph_src) $(subgraph)/ops/build.sh
	$(log_start)
	$(ipfs_start)
	$(docker_run_in_subgraph) "bash ops/build.sh 4447"
	$(ipfs_stop)
	$(log_finish) && touch build/subgraph

subgraph-prod: sugraph-node-modules contract-artifacts $(subgraph_src) $(subgraph)/ops/build.sh
	$(log_start)
	$(ipfs_start)
	$(docker_run_in_subgraph) "bash ops/build.sh 1"
	$(ipfs_stop)
	$(log_finish) && touch build/subgraph-prod

sugraph-node-modules: builder $(subgraph)/package.json
	$(log_start)
	$(docker_run_in_subgraph) "yarn install"
	$(log_finish) && touch build/sugraph-node-modules

# Etherum provider: ganache + truffle

ethprovider: contract-artifacts $(contract_ops)
	$(log_start)
	docker build --file $(contracts)/ops/dev.dockerfile --tag $(ethprovider_image):dev $(contracts)
	$(log_finish) && touch build/ethprovider

contract-artifacts: contract-node-modules $(contract_src)
	$(log_start)
	$(docker_run_in_contracts) "yarn build"
	$(log_finish) && touch build/contract-artifacts

contract-node-modules: builder $(contract_src)
	$(log_start)
	$(docker_run_in_contracts) "yarn install"
	$(log_finish) && touch build/contract-node-modules

# Builder
builder: $(shell find $(cwd)/ops -not -name "deploy.*" $(find_opts))
	$(log_start) && echo $?
	docker build --file ops/builder.dockerfile --tag $(project)_builder:dev .
	$(log_finish) && touch build/builder
